﻿using System.Linq;

namespace Oits.Web.Profile { 

	public abstract class DbProfileProviderBase : System.Web.Profile.ProfileProvider { 

		#region fields
		private System.String myApplicationName;
		private System.String myDbMapGroupName;
		#endregion fields


		#region .ctor
		protected DbProfileProviderBase() : base() { 
		}
		#endregion .ctor


		#region properties
		public override System.String ApplicationName { 
			get { 
				return myApplicationName;
			}
			set { 
				value = value.TrimToNull();
				if ( System.String.IsNullOrEmpty( value ) ) { 
					throw new System.ArgumentException( "ApplicationName property may not be null or empty.", "ApplicationName" );
				}
				myApplicationName = value;
			}
		}
		public virtual Oits.Configuration.DbMap.DbMapGroupElement DbMapGroup { 
			get {
				return Oits.Configuration.DbMap.DbMapSection.GetSection().Groups[ this.DbMapGroupName ];
			}
		}
		public virtual System.String DbMapGroupName { 
			get { 
				return myDbMapGroupName;
			}
		}
		#endregion properties


		#region methods
		public override void Initialize( System.String name, System.Collections.Specialized.NameValueCollection config ) { 
			base.Initialize( name, config );

			System.String configValue;

			configValue = config[ "applicationName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) { 
				configValue = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
			}
			myApplicationName = configValue;
			config.Remove( "applicationName" );

			configValue = config[ "dbMapGroupName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) { 
				throw new System.Configuration.ConfigurationErrorsException( "The dbMapGroupName attribute was not found, or was null, or was empty." );
			} else { 
				myDbMapGroupName = configValue;
			}
			config.Remove( "dbMapGroupName" );
		}

		protected abstract System.Configuration.SettingsPropertyValueCollection GetPropertyValues( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Configuration.SettingsContext context, System.Configuration.SettingsPropertyCollection collection, Oits.Configuration.DbMap.DbCommandElement command, Oits.Configuration.DbMap.DbResultElement result );
		protected abstract void SetPropertyValues( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Configuration.SettingsContext context, System.Configuration.SettingsPropertyValueCollection collection, Oits.Configuration.DbMap.DbCommandElement command, Oits.Configuration.DbMap.DbResultElement result );

		protected abstract System.Int32 DeleteInactiveProfiles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Profile.ProfileAuthenticationOption authenticationOption, System.DateTime userInactiveSinceDate, Oits.Configuration.DbMap.DbCommandElement command );
		protected abstract System.Int32 DeleteProfiles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> usernames, Oits.Configuration.DbMap.DbCommandElement command );
		protected abstract System.Int32 DeleteProfiles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Profile.ProfileInfoCollection profiles, Oits.Configuration.DbMap.DbCommandElement command );
		public abstract System.Web.Profile.ProfileInfoCollection FindInactiveProfilesByUserName( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Profile.ProfileAuthenticationOption authenticationOption, System.String usernameToMatch, System.DateTime userInactiveSinceDate, System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords, Oits.Configuration.DbMap.DbCommandElement command, Oits.Configuration.DbMap.DbResultElement result );
		public abstract System.Web.Profile.ProfileInfoCollection FindProfilesByUserName( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Profile.ProfileAuthenticationOption authenticationOption, System.String usernameToMatch, System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords, Oits.Configuration.DbMap.DbCommandElement command, Oits.Configuration.DbMap.DbResultElement result );
		public abstract System.Web.Profile.ProfileInfoCollection GetAllInactiveProfiles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Profile.ProfileAuthenticationOption authenticationOption, System.DateTime userInactiveSinceDate, System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords, Oits.Configuration.DbMap.DbCommandElement command, Oits.Configuration.DbMap.DbResultElement result );
		public abstract System.Web.Profile.ProfileInfoCollection GetAllProfiles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Profile.ProfileAuthenticationOption authenticationOption, System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords, Oits.Configuration.DbMap.DbCommandElement command, Oits.Configuration.DbMap.DbResultElement result );
		protected abstract System.Int32 GetNumberOfInactiveProfiles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Profile.ProfileAuthenticationOption authenticationOption, System.DateTime userInactiveSinceDate, Oits.Configuration.DbMap.DbCommandElement command );
		#endregion methods

	}

}