﻿using System.Linq;

namespace Oits.Web.Profile { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Web.Profile.ExtensionMethods"]/member[@name=""]/*'/>
	public static class ExtensionMethods { 

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Web.Profile.ExtensionMethods"]/member[@name="TrimToNull(System.String)"]/*'/>
		public static System.String TrimToNull( this System.String @string ) { 
			if ( System.String.IsNullOrEmpty( @string ) ) { 
				return null;
			}
			@string = @string.Trim();
			if ( System.String.IsNullOrEmpty( @string ) ) { 
				return null;
			}
			return @string;
		}

	}

}